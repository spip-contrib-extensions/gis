<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function gis_lire_meta_exif_geo(string $fichier): ?array {
	$data = null;

	if (
		function_exists('exif_read_data')
		&& ($exifs = exif_read_data(_DIR_IMG . $fichier))
		&& isset($exifs['GPSLatitude'])
	) {
		spip_log("GIS EXIFS : récupération des coordonnées du fichier $fichier", 'gis');

		$data['GPSLatitude'] = $exifs['GPSLatitude'];
		$data['GPSLatitudeRef'] = $exifs['GPSLatitudeRef'];
		$data['GPSLongitude'] = $exifs['GPSLongitude'];
		$data['GPSLongitudeRef'] = $exifs['GPSLongitudeRef'];
	}

	return $data;
}

function gis_lire_meta_iptc_geo(string $fichier): ?array {
	$data = null;
	include_spip('inc/iptc');

	$er = new class_IPTC($fichier);
	$iptc = $er->fct_lireIPTC();

	if (
		$iptc
		&& is_array($iptc)
		&& (!empty($iptc['city']) || !empty($iptc['country']) || !empty($iptc['provinceState']))
	) {
		$data = [];
		$data['city'] = $iptc['city'];
		$data['country'] = $iptc['country'];
		$data['provinceState'] = $iptc['provinceState'];
	}

	return $data;
}


/**
 * Géolocaliser une photo (jpg ou tiff) selon ses métadonnées (exif ou iptc) si elle en possède
 *
 * @param int $id_document
 * @param string $fichier
 * @param bool $maj          Permet de définir si l'on souhaite mettre à jour le premier point GIS associé ou non.
 *                           Dans le cas où l'on n'aurait pas activé la géolocalisation des documents, associé des points GIS
 *                         puis déclenché la géolocalisation auto, on ne veut sans doute pas perdre les points GIS précédents.
 * @return void
 * @throws JsonException
 */
function gis_geolocaliser_image_selon_meta(int $id_document, string $fichier, bool $maj = true): void {
	$adresse = $code_postal = $ville = $region = $pays = $code_pays = '';
	$latitude = $longitude = null;

	include_spip('inc/config');
	$config = lire_config('gis', []);

	// On commence par chercher des données EXIFS de géolocalisation
	if ($exifs = gis_lire_meta_exif_geo($fichier)) {
		if (!function_exists('dms_to_dec')) {
			include_spip('gis_fonctions');
		}

		$lat_deg = explode('/', $exifs['GPSLatitude'][0]);
		$int_lat_deg = (float) $lat_deg[0] / (float) ($lat_deg[1] ?: 1);

		$lat_min = explode('/', $exifs['GPSLatitude'][1]);
		$int_lat_min = (float) $lat_min[0] / (float) ($lat_min[1] ?: 1);

		$lat_sec = explode('/', $exifs['GPSLatitude'][2]);
		$int_lat_sec = (float) $lat_sec[0] / (float) ($lat_sec[1] ?: 1);

		$lon_deg = explode('/', $exifs['GPSLongitude'][0]);
		$int_lon_deg = (float) $lon_deg[0] / (float) ($lon_deg[1] ?: 1);

		$lon_min = explode('/', $exifs['GPSLongitude'][1]);
		$int_lon_min = (float) $lon_min[0] / (float) ($lon_min[1] ?: 1);

		$lon_sec = explode('/', $exifs['GPSLongitude'][2]);
		$int_lon_sec = (float) $lon_sec[0] / (float) ($lon_sec[1] ?: 1);

		// round to 5 = approximately 1 meter accuracy
		if (is_numeric($int_lat_deg) && is_numeric($int_lat_min) && is_numeric($int_lat_sec)) {
			$latitude = round(dms_to_dec($exifs['GPSLatitudeRef'], $int_lat_deg, $int_lat_min, $int_lat_sec), 5);
		}

		if (is_numeric($int_lon_deg) && is_numeric($int_lon_min) && is_numeric($int_lon_sec)) {
			$longitude =  round(dms_to_dec($exifs['GPSLongitudeRef'], $int_lon_deg, $int_lon_min, $int_lon_sec), 5);
		}

		if (
			$config['geocoder'] === 'on'
			&& $latitude
			&& $longitude
		) {
			include_spip('inc/gis_geocode');
			$json = gis_geocode_request('reverse', [
				'format' => 'json',
				'addressdetails' => 1,
				'accept-language' => $GLOBALS['meta']['langue_site'],
				'lat' => $latitude,
				'lon' => $longitude
			]);
			$geocoder = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
			if (is_array($geocoder)) {
				$pays = $geocoder['address']['country'];
				$code_pays = $geocoder['address']['country_code'];
				$region = $geocoder['address']['state'];
				if ($geocoder['address']['city']) {
					$ville = $geocoder['address']['city'];
				} elseif ($geocoder['address']['town']) {
					$ville = $geocoder['address']['town'];
				} elseif ($geocoder['address']['village']) {
					$ville = $geocoder['address']['village'];
				}
				$code_postal = $geocoder['address']['postcode'];
				$adresse = $geocoder['address']['road'];
			}
		}
	} else {
		// Sinon, on essaie les données IPTC
		$string_recherche = '';

		if ($iptc = gis_lire_meta_iptc_geo($fichier)) {
			if ($iptc['city']) {
				$string_recherche .= $iptc['city'] . ', ';
			}
			if ($iptc['provinceState']) {
				$string_recherche .= $iptc['provinceState'] . ', ';
			}
			if ($iptc['country']) {
				$string_recherche .= $iptc['country'];
			}
		}

		if (strlen($string_recherche)) {
			include_spip('inc/gis_geocode');
			$json = gis_geocode_request('search', [
				'format' => 'json',
				'addressdetails' => 1,
				'limit' => 1,
				'accept-language' => $GLOBALS['meta']['langue_site'],
				'q' => $string_recherche
			]);
			$geocoder = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
			if (is_array($geocoder[0])) {
				$latitude = $geocoder[0]['lat'];
				$longitude = $geocoder[0]['lon'];
				if ($config['adresse'] == 'on') {
					$pays = $geocoder[0]['address']['country'];
					$code_pays = $geocoder[0]['address']['country_code'];
					$region = $geocoder[0]['address']['state'];
					if ($geocoder[0]['address']['city']) {
						$ville = $geocoder[0]['address']['city'];
					} elseif ($geocoder[0]['address']['town']) {
						$ville = $geocoder[0]['address']['town'];
					} elseif ($geocoder[0]['address']['village']) {
						$ville = $geocoder[0]['address']['village'];
					}
				}
			}
		}
	}

	if (
		$latitude
		&& is_numeric($latitude)
		&& $longitude
		&& is_numeric($longitude)
	) {
		$c = [
			'titre' => basename($fichier),
			'lat' => $latitude,
			'lon' => $longitude,
			'zoom' => $config['zoom'] ?: '4',
			'adresse' => $adresse,
			'code_postal' => $code_postal,
			'ville' => $ville,
			'region' => $region,
			'pays' => $pays,
			'code_pays' => $code_pays
		];

		if (defined('_DIR_PLUGIN_GISGEOM')) {
			$geojson = '{"type":"Point","coordinates":[' . $longitude . ',' . $latitude . ']}';
			set_request('geojson', $geojson);
		}

		include_spip('action/editer_objet');
		include_spip('action/editer_gis');

		if (
			$maj
			&& ($id_gis = sql_getfetsel('G.id_gis', 'spip_gis AS G LEFT JOIN spip_gis_liens AS T ON T.id_gis = G.id_gis', "T.id_objet = $id_document AND T.objet='document'"))
		) {
			// Des coordonnées sont déjà définies pour ce document => on les update
			objet_modifier('gis', $id_gis, $c);
			spip_log("GIS EXIFS : update des coordonnées depuis EXIFS pour le document $id_document => id_gis = $id_gis", 'gis');
		} else {
			// Aucune coordonnée n'est définie pour ce document => on les crée
			$id_gis = objet_inserer('gis');
			objet_modifier('gis', $id_gis, $c);
			gis_associer($id_gis, ['document' => $id_document]);
			spip_log("GIS EXIFS : création des coordonnées depuis EXIFS pour le document $id_document => id_gis = $id_gis", 'gis');
		}
	}
}
