<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Récupérer les données depuis le geocoder (Nomatim ou Photon)
 *
 * @param string $mode
 *    mode de geocoding : search ou reverse
 * @param array $arguments
 *    liste des arguments de la requête : format, q, limit, addressdetails, accept-language, lat, lon
 * @return string données renvoyées par le geocoder
 */
function gis_geocode_request($mode, $arguments = []) {

	if (!$mode or !in_array($mode, ['search', 'reverse'])) {
		return '';
	}

	$geocoder = defined('_GIS_GEOCODER') ? _GIS_GEOCODER : 'photon';

	if ($geocoder == 'photon') {
		unset($arguments['format']);
		unset($arguments['addressdetails']);
	}

	if (!empty($arguments) and in_array($geocoder, ['photon','nominatim'])) {
		if ($geocoder == 'photon') {
			if (isset($arguments['accept-language'])) {
				// ne garder que les deux premiers caractères du code de langue, car les variantes spipiennes comme fr_fem posent problème
				$arguments['lang'] = substr($arguments['accept-language'], 0, 2);
				unset($arguments['accept-language']);
			}
			if ($mode == 'search') {
				$mode = 'api/';
			} else {
				$mode = 'reverse';
			}
			$url = 'http://photon.komoot.io/';
		} else {
			$url = 'http://nominatim.openstreetmap.org/';
		}

		$url = defined('_GIS_GEOCODER_URL') ? _GIS_GEOCODER_URL : $url;
		include_spip('inc/distant');
		$data = recuperer_url("{$url}{$mode}?" . http_build_query($arguments));
		return $data['page'];
	} else {
		return '';
	}
}
