<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Formulaire de création et d'édition d'un point géolocalisé
 */

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Chargement des valeurs par défaut du formulaire
 *
 * @param int|string $id_gis Identifiant numérique du point ou 'new' pour un nouveau
 * @param string $objet Le type d'objet SPIP auquel il est attaché
 * @param int|string $id_objet L'id_objet de l'objet auquel il est attaché
 * @param string $retour L'url de retour
 * @param string $ajaxload initialiser la carte à chaque onAjaxLoad()
 * @param array|string $options TODO à documenter, voir avec l'auteur de https://zone.spip.org/trac/spip-zone/changeset/53906
 */
function formulaires_editer_gis_charger_dist($id_gis = 'new', $objet = '', $id_objet = '', $retour = '', $ajaxload = 'oui', $options = '') {
	$valeurs = formulaires_editer_objet_charger('gis', $id_gis, '', '', $retour, '');
	$valeurs['objet'] = $objet;
	$valeurs['id_objet'] = $id_objet;
	$valeurs['ajaxload'] = $ajaxload;
	/* Traitement des options */
	/* peut etre a envoyer dans une fonction generique de verification des options */
	if (is_array($options)) {
		if (!empty($options['titre']) and (isset($options['titre']) and is_string($options['titre']))) {
			$valeurs['titre'] = $options['titre'];
		}
		if (!empty($valeurs['lat']) and (isset($options['lat']) and is_numeric($options['lat']))) {
			$valeurs['lat'] = $options['lat'];
		}
		if (!empty($valeurs['lon']) and (isset($options['lon']) and is_numeric($options['lon']))) {
			$valeurs['lon'] = $options['lon'];
		}
		if (!empty($valeurs['zoom']) and (isset($options['zoom']) and is_numeric($options['zoom']) and intval($options['zoom']) == $options['zoom'])) {
			$valeurs['zoom'] = $options['zoom'];
		}
		/* Bounding Box */
		if (!empty($valeurs['sw_lat']) and (isset($options['sw_lat']) and is_numeric($options['sw_lat']))) {
			$valeurs['sw_lat'] = $options['sw_lat'];
		}
		if (!empty($valeurs['sw_lon']) and (isset($options['sw_lon']) and is_numeric($options['sw_lon']))) {
			$valeurs['sw_lon'] = $options['sw_lon'];
		}
		if (!empty($valeurs['ne_lat']) and (isset($options['ne_lat']) and is_numeric($options['ne_lat']))) {
			$valeurs['ne_lat'] = $options['ne_lat'];
		}
		if (!empty($valeurs['ne_lon']) and (isset($options['ne_lon']) and is_numeric($options['ne_lon']))) {
			$valeurs['ne_lon'] = $options['ne_lon'];
		}
	}
	return $valeurs;
}

/**
 * Vérification des valeurs du formulaire
 *
 * 4 champs sont obligatoires :
 * -* Son titre
 * -* Sa latitude
 * -* Sa longitude
 * -* Son niveau de zoom
 *
 * @param int|string $id_gis Identifiant numérique du point ou 'new' pour un nouveau
 * @param string $objet Le type d'objet SPIP auquel il est attaché
 * @param int|string $id_objet L'id_objet de l'objet auquel il est attaché
 * @param string $retour L'url de retour
 * @param string $ajaxload initialiser la carte à chaque onAjaxLoad()
 * @param array|string $options TODO à documenter, voir avec l'auteur de https://zone.spip.org/trac/spip-zone/changeset/53906
 */
function formulaires_editer_gis_verifier_dist($id_gis = 'new', $objet = '', $id_objet = '', $retour = '', $ajaxload = 'oui', $options = '') {
	$erreurs = formulaires_editer_objet_verifier('gis', $id_gis, ['titre', 'lat', 'lon', 'zoom']);
	return $erreurs;
}

/**
 * Traitement des valeurs du formulaire
 *
 * @param int|string $id_gis Identifiant numérique du point ou 'new' pour un nouveau
 * @param string $objet Le type d'objet SPIP auquel il est attaché
 * @param int|string $id_objet L'id_objet de l'objet auquel il est attaché
 * @param string $retour L'url de retour
 * @param string $ajaxload initialiser la carte à chaque onAjaxLoad()
 * @param array|string $options TODO à documenter, voir avec l'auteur de https://zone.spip.org/trac/spip-zone/changeset/53906
 */
function formulaires_editer_gis_traiter_dist($id_gis = 'new', $objet = '', $id_objet = '', $retour = '', $ajaxload = 'oui', $options = '') {
		return formulaires_editer_objet_traiter('gis', $id_gis, '', '', $retour, '');
}
