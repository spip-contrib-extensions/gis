<?php

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class GisGeolocaliserImages extends Command {
	protected function configure() {
		$this
			->setName('gis:geolocaliser:images')
			->setDescription('Créer et associer automatiquement un point GIS selon les données EXIF ou IPTC des images JPG ou TIFF')
			->addOption(
				'toutes',
				null,
				InputOption::VALUE_OPTIONAL,
				'Traiter toutes les images JPG ou TIFF, y compris celles déjà associées à au moins un point GIS',
				false
			)
			->addOption(
				'maj',
				null,
				InputOption::VALUE_OPTIONAL,
				'En conjonction avec --toutes, mettre à jour le premier point GIS éventuellement associé à l\'image',
				false
			)
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$toutes = $input->getOption('toutes') !== false;
		$maj = $input->getOption('maj') !== false;

		include_spip('base/abstract_sql');
		include_spip('inc/gis_metadata');

		$progress_bar = new ProgressBar($output);

		$docs = sql_allfetsel(
			[
				'SD.id_document',
				'SD.fichier',
			],
			'spip_documents SD',
			sql_in('SD.extension', ['jpg', 'tiff'])
		);

		if ($toutes) {
			$a_traiter = $docs;
		} else {
			$avec_gis = sql_allfetsel(
				'DISTINCT SGL.id_objet',
				'spip_gis_liens SGL',
				[
					'objet = ' . sql_quote('document')
				]
			);

			$avec_gis = array_map(function ($item) {
				return $item['id_objet'];
			}, $avec_gis);

			$a_traiter = array_filter($docs, function ($v) use ($avec_gis) {
				return !in_array($v['id_document'], $avec_gis);
			}, ARRAY_FILTER_USE_BOTH);
		}

		foreach ($progress_bar->iterate($a_traiter) as $image) {
			gis_geolocaliser_image_selon_meta(intval($image['id_document']), _DIR_IMG . $image['fichier'], $maj);
		}

		$progress_bar->finish();

		return Command::SUCCESS;
	}
}
