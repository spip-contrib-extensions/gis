# Changelog

## Unreleased

## 5.5.0 - 2025-03-03

### Added

- !80 Intégration du script Leaflet.GestureHandling pour que les cartes ne capturent plus le scroll

### Fixed

- `#LARGEUR_ECRAN` est dépréciée en 4.4 et supprimée en 5.0
- Ajout du fond de carte CyclOSM
- !77 Retrait du fond de carte Esri.DeLorme

## 5.4.0 - 2024-10-03

### Added

- #78 Permettre de ne pas insérer le CSS/JS côté public

### Changed

- !73 Dans `gis_icon_properties` utiliser `focus` plutôt que `center`

## 5.3.0 - 2024-07-28

### Added

- #22 Permettre de géolocaliser à la demande les images JPG ou TIFF selon leurs métadonnées EXIF ou IPTC

## 5.2.0 - 2024-07-05

### Changed

- Compatibilité SPIP 4.*

## 5.1.2 - 2024-05-24

### Fixed

- #77 Réparer la géolocations d'une image selon ses exfis

## 5.1.1 - 2024-05-23

### Fixed

- !63 Réparer l'usage des couches stamen dans l'API de cartes statiques
- #77 Ne pas générer de warnings  lors de la géolocalisation d'une image selon ses exfis
- #64 Rétablir le peuplement de country_code quand on utilise le geocoder photon
- Éviter une erreur de squelette avec SPIP 5

## 5.1.0 - 2023-09-28

### Fixed

- !63 Bascule des couches Stamen vers leur nouveau fournisseur Stadia Maps qui nécessite la création d'un compte cf http://maps.stamen.com/stadia-partnership/
- Se prémunir d'une fatale dans le form de config si aucune couche n'est sélectionnée dans le champ "Couches proposées"
- !62 Retirer les CDATA inutiles
- Ne pas générer d'erreur dans l'API des cartes statiques quand un provider renvoie une erreur pour une tile


## 5.0.0 - 2023-03-08

### Changed

- !54 Plus besoin d'assurer le fallback sur PclZip maintenant que le plugin est compatible SPIP 4 mini
- Compatiblité SPIP 4.1 minimum

### Removed

- !54 Retrait des fonctions dépréciées `gis_inserer()`, `lier_gis() & `delier_gis()`
- !54 Retrait de tout ce qui concerne les plugins XMLRPC & CRUD

### Fixed

- !54 Modifier la valeur de retour de `gis_geocode_request` de `false` à `''` en cas d'échec
- !54 Évivter un deprecated sur `generer_url_entite()` => `generer_objet_url()`
