#!/bin/bash
declare -a js=(
    "https://raw.githubusercontent.com/shramov/leaflet-plugins/master/layer/tile/Bing.js"
    "https://raw.githubusercontent.com/brunob/leaflet.fullscreen/master/Control.FullScreen.js"
    "https://raw.githubusercontent.com/Norkart/Leaflet-MiniMap/master/src/Control.MiniMap.js"
    "https://unpkg.com/leaflet.gridlayer.googlemutant@latest/dist/Leaflet.GoogleMutant.js"
    "https://raw.githubusercontent.com/shramov/leaflet-plugins/master/layer/vector/GPX.js"
    "https://raw.githubusercontent.com/shramov/leaflet-plugins/master/layer/vector/GPX.Speed.js"
    "https://raw.githubusercontent.com/shramov/leaflet-plugins/master/layer/vector/KML.js"
    "https://unpkg.com/leaflet.markercluster@latest/dist/leaflet.markercluster-src.js"
    "https://raw.githubusercontent.com/leaflet-extras/leaflet-providers/master/leaflet-providers.js"
    "https://raw.githubusercontent.com/shramov/leaflet-plugins/master/layer/Marker.Rotate.js"
    "https://raw.githubusercontent.com/shramov/leaflet-plugins/master/layer/vector/TOPOJSON.js"
    "https://raw.githubusercontent.com/Raruto/leaflet-gesture-handling/refs/heads/master/dist/leaflet-gesture-handling.js"
);
for i in "${js[@]}"
do
	wget $i -O "plugins/$( basename $i )"
done
declare -a images=(
    "https://raw.githubusercontent.com/brunob/leaflet.fullscreen/master/icon-fullscreen.svg"
    "https://raw.githubusercontent.com/Norkart/Leaflet-MiniMap/master/src/images/toggle.png"
    "https://raw.githubusercontent.com/Norkart/Leaflet-MiniMap/master/src/images/toggle.svg"
);
for i in "${images[@]}"
do
	wget $i -O "plugins/images/$( basename $i )"
done

wget -O plugins/leaflet.markercluster.css https://unpkg.com/leaflet.markercluster@latest/dist/MarkerCluster.css https://unpkg.com/leaflet.markercluster@latest/dist/MarkerCluster.Default.css

# todo dans plugins/leaflet-plugins.css avec ajout de images/ aux url(xxx)
# https://raw.githubusercontent.com/brunob/leaflet.fullscreen/master/Control.FullScreen.css
# https://raw.githubusercontent.com/Norkart/Leaflet-MiniMap/master/src/Control.MiniMap.css
# https://raw.githubusercontent.com/Raruto/leaflet-gesture-handling/refs/heads/master/dist/leaflet-gesture-handling.css