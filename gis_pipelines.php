<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Insertion des css du plugin dans les pages publiques
 *
 * @param string $flux
 * @return string
 */
function gis_insert_head_css($flux) {
	if (lire_config('gis/no_asset', '')) {
		return $flux;
	}

	$flux .= "\n" . '<link rel="stylesheet" href="' . direction_css(sinon(find_in_path('css/leaflet.css'), find_in_path('lib/leaflet/dist/leaflet.css'))) . '" />';
	$flux .= "\n" . '<link rel="stylesheet" href="' . direction_css(sinon(find_in_path('css/leaflet-plugins.css'), find_in_path('lib/leaflet/plugins/leaflet-plugins.css'))) . '" />';
	$flux .= "\n" . '<link rel="stylesheet" href="' . direction_css(sinon(find_in_path('css/leaflet.markercluster.css'), find_in_path('lib/leaflet/plugins/leaflet.markercluster.css'))) . '" />';
	$flux .= "\n" . '<link rel="stylesheet" href="' . sinon(find_in_path('css/leaflet_nodirection.css'), find_in_path('lib/leaflet/dist/leaflet_nodirection.css')) . '" />';
	return $flux;
}

/**
 * Insertion des scripts du plugin dans les pages publiques
 *
 * @param string $flux
 * @return string
 */
function gis_insert_head($flux) {
	// initialisation des valeurs de config
	include_spip('inc/config');
	$config = lire_config('gis', []);

	if ($config['gis']['no_asset'] ?? '') {
		return $flux;
	}

	if (!isset($config['layers']) || !is_array($config['layers'])) {
		$config['layers'] = ['openstreetmap_mapnik'];
	}

	include_spip('gis_fonctions');
	if (!in_array(gis_layer_defaut(), $config['layers'])) {
		$config['layers'][] = gis_layer_defaut();
	}

	// insertion des scripts pour google si nécessaire
	if (count(array_intersect(['google_roadmap', 'google_satellite', 'google_terrain'], $config['layers'])) > 0) {
		$flux .= "\n" . '<script type="text/javascript" src="//maps.google.com/maps/api/js?language=' . $GLOBALS['spip_lang'] . '&amp;key=' . ($config['api_key_google'] ?? '') . '"></script>';
	}

	return $flux;
}

/**
 * Insertion des scripts et css du plugin dans les pages de l'espace privé
 *
 * @param string $flux
 * @return string
 */
function gis_header_prive($flux) {
	$flux .= gis_insert_head_css('');
	$flux .= gis_insert_head('');
	return $flux;
}

/**
 * Insertion du bloc GIS dans les pages des objets de l'espace privé
 *
 * @param array $flux
 * @return array
 */
function gis_afficher_contenu_objet($flux) {
	if (
		$objet = $flux['args']['type']
		and include_spip('inc/config')
		and in_array(table_objet_sql($objet), lire_config('gis/gis_objets', []))
		and ($id = intval($flux['args']['id_objet']))
	) {
		$texte = recuperer_fond(
			'prive/contenu/gis_objet',
			[
				'table_source' => 'gis',
				'objet' => $objet,
				'id_objet' => $id
			]
		);
		$flux['data'] .= $texte;
	}

	return $flux;
}

/**
 * Si la geolocalisation des documents est activée dans la config,
 * création/suppression d'un point à partir des métadonnées du document ajouté (jpg, kml et kmz)
 *
 * @param array $flux
 * @return array
 */
function gis_post_edition($flux) {
	if (
		is_array($flux) && isset($flux['args']['operation']) && ($flux['args']['operation'] == 'ajouter_document')
		and ($document = sql_fetsel('*', 'spip_documents', 'id_document=' . intval($flux['args']['id_objet'])))
		and (in_array(table_objet_sql('document'), lire_config('gis/gis_objets', [])))
	) {
		if (in_array($document['extension'], ['jpg', 'tiff', 'kml', 'kmz'])) {
			$config = lire_config('gis', []);

			include_spip('inc/documents');
			$fichier = get_spip_doc($document['fichier']);
			$id_document = intval($document['id_document']);

			if (
				file_exists($fichier)
				&& in_array($document['extension'], ['jpg', 'tiff'])
			) {
				include_spip('inc/gis_metadata');
				gis_geolocaliser_image_selon_meta($id_document, $fichier);
			} elseif (in_array($document['extension'], ['kml','kmz','gpx'])) {
				$recuperer_info = charger_fonction('kml_infos', 'inc');
				$infos = $recuperer_info($document['id_document']);
				if ($infos) {
					if (is_numeric($latitude = $infos['latitude']) && is_numeric($longitude = $infos['longitude'])) {
						$c = [
							'titre' => $infos['titre'] ?: basename($fichier),
							'descriptif' => $infos['descriptif'],
							'lat' => $latitude,
							'lon' => $longitude,
							'zoom' => $config['zoom'] ?: '4'
						];

						include_spip('action/editer_objet');
						include_spip('action/editer_gis');

						if ($id_gis = sql_getfetsel('G.id_gis', 'spip_gis AS G LEFT  JOIN spip_gis_liens AS T ON T.id_gis=G.id_gis', 'T.id_objet=' . intval($id_document) . " AND T.objet='document'")) {
							// Des coordonnées sont déjà définies pour ce document => on les update
							objet_modifier('gis', $id_gis, $c);
							spip_log("GIS EXIFS : Update des coordonnées depuis EXIFS pour le document $id_document => id_gis = $id_gis", 'gis');
						} else {
							// Aucune coordonnée n'est définie pour ce document  => on les crées
							$id_gis = objet_inserer('gis');
							objet_modifier('gis', $id_gis, $c);
							gis_associer($id_gis, ['document' => $id_document]);
							spip_log("GIS EXIFS : Création des coordonnées depuis EXIFS pour le document $id_document => id_gis = $id_gis", 'gis');
						}
					}
					unset($infos['longitude']);
					unset($infos['latitude']);
					if ((is_countable($infos) ? count($infos) : 0) > 0) {
						include_spip('action/editer_objet');
						objet_modifier('document', $id_document, $infos);
					}
				}
			}
		}
	}
	if (
		is_array($flux) && isset($flux['args']['operation']) && ($flux['args']['operation'] == 'supprimer_document')
		and ($id_document = intval($flux['args']['id_objet'])
		and ($id_gis = sql_getfetsel('G.id_gis', 'spip_gis AS G LEFT JOIN spip_gis_liens AS T ON T.id_gis=G.id_gis', 'T.id_objet=' . intval($id_document) . " AND T.objet='document'")))
	) {
		include_spip('action/editer_gis');
		gis_supprimer($id_gis);
		spip_log("GIS EXIFS : Suppression des coordonnées pour le document $id_document => id_gis = $id_gis", 'gis');
	}

	return $flux;
}


/**
 * Optimiser la base de données en supprimant les liens orphelins
 *
 * @param array $flux
 * @return array
 */
function gis_optimiser_base_disparus($flux) {

	include_spip('action/editer_liens');
	// optimiser les liens morts :
	// entre gis vers des objets effaces
	// depuis des gis effaces
	$flux['data'] += objet_optimiser_liens(['gis' => '*'], '*');

	return $flux;
}

function gis_saisies_autonomes($flux) {
	$flux[] = 'carte';
	return $flux;
}

/**
 * Modifier le contenu de la compilation d'un squelette donné.
 */
function gis_recuperer_fond(array $flux): array {
	// Surcharger le formulaire editer_lien pour ajout d'un bouton de géolocalisation automatisée selon données EXIF ou IPTC d'un jpg ou d'un tiff
	if (
		$flux['args']['fond'] === 'formulaires/editer_liens'
		&& $flux['args']['contexte']['objet'] === 'document'
		&& $flux['args']['contexte']['objet_source'] === 'gis'
	) {
		$id_document = intval($flux['args']['contexte']['id_objet']);
		$document = sql_fetsel(['extension', 'fichier'], 'spip_documents', "id_document=$id_document");
		if (in_array($document['extension'], ['jpg', 'tiff'])) {
			include_spip('inc/gis_metadata');

			if (
				gis_lire_meta_exif_geo(_DIR_IMG . $document['fichier'])
				|| gis_lire_meta_iptc_geo(_DIR_IMG . $document['fichier'])
			) {
				$url = generer_action_auteur('gis_geolocaliser_image_selon_metadonnees', $flux['args']['contexte']['id_objet'], $flux['args']['contexte']['action']);
				$flux['data']['texte'] = preg_replace('/<!--ajaxbloc-->/', "<!--ajaxbloc--><span class='bouton_document_gis_auto'><a class='btn btn_mini' href='$url' title='" . _T('gis:title_creer_gis_metadonnees') . "'>" . _T('gis:texte_creer_gis_metadonnees') . '</a></span>', $flux['data']['texte'], 1);
			}
		}
	}

	return $flux;
}

/**
 * Insertion dans le traitement du formulaire de configuration
 *
 * Purger le répertoire js si on a une carte google dans les layers pour recalculer le js statique
 * Peut être à améliorer
 * Invalider le cache lors de l'ajout ou dissociation d'un point à un objet, "Voir en ligne" ne suffit pas
 * car le json est sur un autre hit
 *
 * @param array $flux
 * 		Le contexte du pipeline
 * @return array $flux
 */
function gis_formulaire_traiter($flux) {
	if (
		$flux['args']['form'] == 'configurer_gis'
		and count(array_intersect(['google_roadmap', 'google_satellite', 'google_terrain'], _request('layers') ?? [])) > 0
	) {
		include_spip('inc/invalideur');
		purger_repertoire(_DIR_VAR . 'cache-js');
		suivre_invalideur(1);
	} elseif (
		$flux['args']['form'] == 'editer_liens'
		and isset($flux['args']['args'][0])
		and $flux['args']['args'][0] == 'gis'
	) {
		include_spip('inc/invalideur');
		suivre_invalideur(1);
	}
	return $flux;
}

/**
 * Definir le libelle pour les logos GIS dans l'espace prive
 * @param array $logo_libelles
 * @return array
 */
function gis_libeller_logo($logo_libelles) {
	$logo_libelles['id_gis'] = _T('gis:libelle_logo_gis');
	return $logo_libelles;
}
