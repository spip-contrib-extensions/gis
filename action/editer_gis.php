<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/autoriser');

function action_editer_gis_dist($arg = null) {
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	include_spip('action/editer_objet');
	// si id_gis n'est pas un nombre, c'est une creation
	if (!$id_gis = intval($arg)) {
		if (!autoriser('creer', 'gis') or !$id_gis = objet_inserer('gis')) {
			return [false, _L('echec')];
		}
	}
	$err = objet_modifier('gis', $id_gis);
	return [$id_gis,$err];
}

/**
 * Enregistrer certaines modifications d'un gis
 *
 * Appelle toutes les fonctions de modification d'un point gis
 * $err est de la forme chaine de langue ou vide si pas d'erreur
 * https://code.spip.net/@articles_set
 *
 * @param int $id_gis l'identifiant numérique du point
 * @param null|array $set un array des valeurs à mettre en base (par défaut false, on récupère les valeurs passées en dans le POST)
 * @return string
 */
function gis_modifier($id_gis, $set = null) {
	include_spip('inc/modifier');
	include_spip('inc/filtres');
	$c = collecter_requests(
		// white list
		objet_info('gis', 'champs_editables'),
		// black list
		['id_objet','objet'],
		// donnees eventuellement fournies
		$set
	);

	if (isset($c['lon'])) {
		if ($c['lon'] > 180) {
			while ($c['lon'] > 180) {
				$c['lon'] = $c['lon'] - 360;
			}
		} elseif ($c['lon'] <= -180) {
			while ($c['lon'] <= -180) {
				$c['lon'] = $c['lon'] + 360;
			}
		}
	}
	if (isset($c['lat'])) {
		if ($c['lat'] > 90) {
			while ($c['lat'] > 90) {
				$c['lat'] = $c['lat'] - 180;
			}
		} elseif ($c['lat'] <= -90) {
			while ($c['lat'] <= -90) {
				$c['lat'] = $c['lat'] + 180;
			}
		}
	}
	if (
		$err = objet_modifier_champs('gis', $id_gis, [
			//'nonvide' => array('nom' => _T('info_sans_titre')),
			'data' => $set,
			'invalideur' => "id='gis/$id_gis'",
		], $c)
	) {
		return $err;
	}

	// lier a un parent ?
	$c = collecter_requests(['id_objet', 'objet'], [], $set);
	if (isset($c['id_objet']) and intval($c['id_objet']) and isset($c['objet']) and $c['objet']) {
		gis_associer($id_gis, [$c['objet'] => $c['id_objet']]);
	}

	return $err;
}


/**
 * Associer un point géolocalisé a des objets listes sous forme
 * array($objet=>$id_objets,...)
 * $id_objets peut lui meme etre un scalaire ou un tableau pour une liste d'objets du meme type
 *
 * on peut passer optionnellement une qualification du (des) lien(s) qui sera
 * alors appliquee dans la foulee.
 * En cas de lot de liens, c'est la meme qualification qui est appliquee a tous
 *
 * @param int $id_gis
 * @param array $objets
 * @param array $qualif
 * @return string
 */
function gis_associer($id_gis, $objets, $qualif = null) {
	include_spip('action/editer_liens');
	$res = objet_associer(['gis' => $id_gis], $objets, $qualif);
	include_spip('inc/invalideur');
	suivre_invalideur("id='gis/$id_gis'");
	return $res;
}

/**
 * Dissocier un point géolocalisé des objets listes sous forme
 * array($objet=>$id_objets,...)
 * $id_objets peut lui meme etre un scalaire ou un tableau pour une liste d'objets du meme type
 *
 * un * pour $id_auteur,$objet,$id_objet permet de traiter par lot
 *
 * @param int $id_gis
 * @param array $objets
 * @return string
 */
function gis_dissocier($id_gis, $objets) {
	include_spip('action/editer_liens');
	$res = objet_dissocier(['gis' => $id_gis], $objets);
	include_spip('inc/invalideur');
	suivre_invalideur("id='gis/$id_gis'");
	return $res;
}

/**
 * Supprimer définitivement un point géolocalisé
 *
 * @param int $id_gis identifiant numérique du point
 * @return int|false 0 si réussite, false dans le cas ou le point n'existe pas
 */
function gis_supprimer($id_gis) {
	$valide = sql_getfetsel('id_gis', 'spip_gis', 'id_gis=' . intval($id_gis));
	if ($valide && autoriser('supprimer', 'gis', $valide)) {
		sql_delete('spip_gis_liens', 'id_gis=' . intval($id_gis));
		sql_delete('spip_gis', 'id_gis=' . intval($id_gis));
		$id_gis = 0;
		include_spip('inc/invalideur');
		suivre_invalideur("id='gis/$id_gis'");
		return $id_gis;
	}
	return false;
}
