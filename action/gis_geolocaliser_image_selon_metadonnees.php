<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action déclenchée au clic sur le bouton de géolocalisation automatisée depuis la page ?exec=document_edit d'un document au format jpg ou tiff
 *
 * @param string|null $arg      l'identifiant du document
 * @return void
 * @throws JsonException
 */
function action_gis_geolocaliser_image_selon_metadonnees_dist(?string $arg = null): void {
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	include_spip('base/abstract_sql');
	include_spip('inc/gis_metadata');

	$id_document = intval($arg);
	$fichier = sql_getfetsel('fichier', 'spip_documents', "id_document = $id_document");

	gis_geolocaliser_image_selon_meta($id_document, _DIR_IMG . $fichier, false);
}
