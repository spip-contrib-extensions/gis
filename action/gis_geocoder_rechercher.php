<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Proxy vers le service du geocoder
 *
 * Cette fonction permet de transmettre une requete auprès du service
 * de recherche d'adresse d'OpenStreetMap (Nomatim ou Photon).
 *
 * Seuls les arguments spécifiques au service sont transmis.
 */
function action_gis_geocoder_rechercher_dist() {
	include_spip('inc/modifier');
	/* On filtre les arguments à renvoyer à Nomatim (liste blanche) */
	$arguments = collecter_requests(['format', 'q', 'limit', 'addressdetails', 'accept-language', 'lat', 'lon'], []);
	include_spip('inc/gis_geocode');
	if ($data = gis_geocode_request(_request('mode'), $arguments)) {
		header('Content-Type: application/json; charset=UTF-8');
		echo $data;
	}
}
