<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


function action_editer_lien_gis_dist() {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();
	[$action, $id_gis, $objet, $id_objet] = explode('/', $arg);

	include_spip('inc/autoriser');
	if (intval($id_gis) and autoriser('lier', 'gis', $id_gis, $GLOBALS['visiteur_session'], ['objet' => $objet,'id_objet' => $id_objet])) {
		include_spip('action/editer_gis');
		if ($action == 'lier') {
			gis_associer($id_gis, [$objet => $id_objet]);
		} elseif ($action == 'delier') {
			gis_dissocier($id_gis, [$objet => $id_objet]);
		}
	}
}
