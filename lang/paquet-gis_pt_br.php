<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-gis?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// G
	'gis_description' => 'Este plugin permite criar pontos geolocalizados que podem ser vinculados aos objetos do SPIP para exibí-los em mapas no seu site. Os mapas podem usar os mosaicos de diferentes fornecedores, graças à biblioteca Leaflet.',
	'gis_slogan' => 'Sistema de informações geográficas.',
];
