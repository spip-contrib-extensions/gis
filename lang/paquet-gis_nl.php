<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-gis?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// G
	'gis_description' => 'Deze plugin laat je locatiepunten maken die aan SPIP-objecten kunnen worden gekoppeld om een kaart te tonen. Verschillende soorten kaarten zijn mogelijk dankzij de Leaflet bibliotheek.',
	'gis_slogan' => 'Geografisch Informatie Systeem',
];
